/* Copyright 2019 Raghu Balasubramanian

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*/

`timescale 1ns / 1ps

`define TEST_REMAINDER_METHOD 1

module gf_multiply_test();

localparam integer Dimension = 8;

logic clk;
logic rst;
logic [Dimension-1:0] operand_x;
logic [Dimension-1:0] operand_y;
logic [Dimension-1:0] result;

// Test stimulus

initial begin
  rst = 1'b1;
  rst = #60 1'b0;
end

initial begin
  clk = 1'b0;
  forever begin
    clk = #15 ~clk;
  end
end

`ifdef TEST_REMAINDER_METHOD
  logic [2*Dimension-2:0][Dimension-1:0] remainders;

  assign remainders[0] = 8'h1;
  assign remainders[1] = 8'h2;
  assign remainders[2] = 8'h4;
  assign remainders[3] = 8'h8;
  assign remainders[4] = 8'h10;
  assign remainders[5] = 8'h20;
  assign remainders[6] = 8'h40;
  assign remainders[7] = 8'h80;
  assign remainders[8] = 8'h1d;
  assign remainders[9] = 8'h3a;
  assign remainders[10] = 8'h74;
  assign remainders[11] = 8'he8;
  assign remainders[12] = 8'hcd;
  assign remainders[13] = 8'h87;
  assign remainders[14] = 8'h13;

  gf_multiply_remainder #(.Dimension(Dimension))
    gf_multiply_remainder(
      .clk(clk),
      .rst(1'b0),
      .remainders(remainders),
      .operand_x(operand_x),
      .operand_y(operand_y),
      .result(result)
    );

`else
  logic [Dimension-1:0] primitive_poly;
  assign primitive_poly = 'h1d;

  gf_multiply_long_form #(.Dimension(Dimension))
    gf_multiply_long_form(
      .clk(clk),
      .rst(1'b0),
      .primitive_poly(primitive_poly),
      .operand_x(operand_x),
      .operand_y(operand_y),
      .result(result)
    );
`endif

logic [Dimension-1:0] expected_result;

typedef struct packed{
  logic [Dimension-1:0] x;
  logic [Dimension-1:0] y;
  logic [Dimension-1:0] result;
} mults_t;

mults_t stimulus_queue[$];
mults_t stimulus_queue_top;

// Stimulus driver
always_ff @(posedge clk) begin
  if (rst) begin
    operand_x <= 0;
    operand_y <= 0;
  end else begin
    if (stimulus_queue.size() == 0) begin
      $display("Test Passed");
      $finish();
    end
    stimulus_queue_top = stimulus_queue.pop_front();
    operand_x <= stimulus_queue_top.x;
    operand_y <= stimulus_queue_top.y;
    expected_result <= stimulus_queue_top.result;
  end
end

// check output
always_ff @(posedge clk) begin
  if (~rst) begin
    if (expected_result != result) begin
      $display("Expected result = 0x%x, Actual = 0x%x", expected_result, result);
      $finish();
    end
  end
end

mults_t one_stimulus;
// stimulus generator
initial begin
  one_stimulus.x = 98;
  one_stimulus.y = 238;
  one_stimulus.result = 72;
  stimulus_queue.push_back(one_stimulus);
  one_stimulus.x = 83;
  one_stimulus.y = 57;
  one_stimulus.result = 26;
  stimulus_queue.push_back(one_stimulus);
  one_stimulus.x = 191;
  one_stimulus.y = 164;
  one_stimulus.result = 93;
  stimulus_queue.push_back(one_stimulus);
  one_stimulus.x = 151;
  one_stimulus.y = 247;
  one_stimulus.result = 34;
  stimulus_queue.push_back(one_stimulus);
  one_stimulus.x = 210;
  one_stimulus.y = 72;
  one_stimulus.result = 96;
  stimulus_queue.push_back(one_stimulus);
  one_stimulus.x = 57;
  one_stimulus.y = 6;
  one_stimulus.result = 150;
  stimulus_queue.push_back(one_stimulus);
  one_stimulus.x = 148;
  one_stimulus.y = 29;
  one_stimulus.result = 159;
  stimulus_queue.push_back(one_stimulus);
  one_stimulus.x = 199;
  one_stimulus.y = 84;
  one_stimulus.result = 64;
  stimulus_queue.push_back(one_stimulus);
  one_stimulus.x = 33;
  one_stimulus.y = 149;
  one_stimulus.result = 194;
  stimulus_queue.push_back(one_stimulus);
  one_stimulus.x = 191;
  one_stimulus.y = 29;
  one_stimulus.result = 215;
  stimulus_queue.push_back(one_stimulus);
end

endmodule
