/* Copyright 2019 Raghu Balasubramanian

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*/

module parallel_gmac_test ();

logic clk;
logic rst;

logic [127:0] h;
logic         h_valid;
logic [3:0][127:0] message;
logic [1:0]   message_words;
logic              message_valid;
logic [127:0] gmac;
logic         gmac_valid;
logic         busy; // will be high for 1 cycle after h_valid.

initial begin
  rst = 1'b1;
  rst = #60 1'b0;
end

initial begin
  clk = 1'b0;
  forever begin
    clk = #15 ~clk;
  end
end

parallel_gmac dut(
  .clk(clk),
  .rst(rst),
  .h(h),
  .h_valid(h_valid),
  .message(message),
  .message_words(message_words),
  .message_valid(message_valid),
  .gmac(gmac),
  .gmac_valid(gmac_valid),
  .busy(busy));

assign message[0] = 128'h00000000_00000000_00000000_00000000;
assign message[1] = 128'h00c00000_00000e00_00030000_00000001;
assign message[2] = 128'h10000000_000a0000_00060000_00050002;
assign message[3] = 128'h8000b000_00000000_00070000_00040003;
assign h = 128'habcdef01_23456789_01234567_89abcdef;
assign message_words = 2'b11; // 4 words.

// First unit test.
typedef enum {idle_st, h_compute_st, message_st} state_t;
state_t next_state;
state_t state;

always_ff @(posedge clk) begin
  if (state == h_compute_st)
    assert(busy == 1'b1);
  else
    assert(busy == 1'b0);
end

always_comb begin
  next_state = state;
  if (~rst) begin
    if (state == idle_st) next_state = h_compute_st;
    if (state == h_compute_st && busy == 1'b0) next_state = message_st;
  end
end

always_ff @(posedge clk) begin
  if (rst) state <= idle_st;
  else state <= next_state;
end

assign h_valid = next_state == h_compute_st && state == idle_st;  // just pulse
assign message_valid = next_state == message_st;

// TODO: check output. Make meaningful tests.

endmodule