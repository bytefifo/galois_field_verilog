add_files ../../galois_field/verilog/xilinx_wrapper.sv
add_files ../../galois_field/verilog/gf_multiply_remainder.sv

synth_design -top xilinx_wrapper -part xc7z020clg400-1

create_clock -add -name sys_clk_pin -period 5.00 [get_ports { clk }]

set_false_path -from [get_ports rst]
set_false_path -from [get_ports input_signal]
set_false_path -to [get_ports output_signal]
