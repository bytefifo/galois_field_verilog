/* Copyright 2019 Raghu Balasubramanian

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*/

// rcon generates the round constant. When the start signal is
// pulsed, it kicks off the generator - which produces the rcon
// for the next cycles.
module rcon (
  input wire clk,
  input wire start,
  input wire act,
  output logic [7:0] rcon
);

// round constant is an 8 bit number that is generated as follows:
// The round constant word array, Rcon[i], contains the values given by
// [xi-1,{00},{00},{00}], with xi-1 being powers of x (x is denoted as {02})
// in the field GF(28), as discussed in Sec. 4.2 (note that i starts at 1,
// not 0).

// This could be statically computed.
// TODO: check if the synthesis tool is smart enuogh or hardcode.
always_ff @(posedge clk) begin
  if (start == 1'b1)
    rcon <= 8'h1;
  else
    rcon <= act ? {rcon[6:0], 1'b0} ^ (8'h1b & {8{rcon[7]}}) : rcon;
end

endmodule
