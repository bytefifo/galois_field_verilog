/* Copyright 2019 Raghu Balasubramanian

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*/

module aes_encrypt_test ();

parameter KeyBytes=16;
parameter aes256 = 1;
parameter NumRounds = aes256 ? 15 : 11;

logic clk;
logic rst;
logic key_init;
logic [KeyBytes-1:0][7:0] key;
logic [KeyBytes-1:0][7:0] key1;
logic do_not_encrypt;
logic [15:0][7:0] data_in;
logic [15:0][7:0] data_out;

initial begin
  clk = 1'b0;
  forever begin
    clk = #15 ~clk;
  end
end

initial begin
  rst = 1'b1;
  rst = #60 1'b0;
end



aes_encrypt #(.KeyBytes(KeyBytes), 
              .NumRounds(NumRounds))
  dut(
    .clk(clk),
    .rst(rst),
    .aes256(aes256),
    .key_init(key_init),
    .key(key),
    .key1(key1),
    .do_not_encrypt(do_not_encrypt),
    .data_in(data_in),
    .data_out(data_out));

int free_running;

always_ff @(posedge clk) begin
  if (rst) free_running <= 0;
  else free_running <= free_running + 1;
end

assign do_not_encrypt = 1'b0;
assign key_init = free_running % 100 == 1;

if(aes256) begin
assign key = 128'h603deb10_15ca71be_2b73aef0_857d7781;
assign key1 = 128'h1f352c07_3b6108d7_2d9810a3_0914dff4;
end else begin
assign key = 128'h2b7e1516_28aed2a6_abf71588_09cf4f3c;
assign key1 = 128'h0;
end
assign data_in = 128'h3243f6a8_885a308d_313198a2_e0370734;

logic [15:0][7:0] expected_encrypted_data_out;
assign expected_encrypted_data_out = 128'h3925841d_02dc09fb_dc118597_196a0b32;

// Checker and exit
always_ff @(posedge clk) begin : scoreboard
  if (free_running == NumRounds + 2) begin
    if (expected_encrypted_data_out == data_out) begin
      $display(" Passed, expected_encrypted_data_out matched data_out");
    end else begin
      $display(" Failed, expected_encrypted_data_out did not match data_out");
      $display(" Expected: %x", expected_encrypted_data_out);
      $display(" Got     : %x", data_out);
    end
    $finish();
  end

  if (free_running > 100) begin
    $display(" Failed, timeout");
    $finish();
  end
end

endmodule
