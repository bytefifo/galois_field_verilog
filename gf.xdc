
create_clock -add -name sys_clk_pin -period 5.00 [get_ports { clk }]

set_false_path -from [get_ports rst]
set_false_path -from [get_ports input_signal]
set_false_path -to [get_ports output_signal]

