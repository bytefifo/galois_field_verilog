/* Copyright 2019 Raghu Balasubramanian

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*/

// One round of AES encryption.

module aes_one_round
  #(parameter IsLastRound=0,
    parameter KeyBytes=16)(
  input wire clk,
  input wire [KeyBytes-1:0][7:0] roundkey,
  input wire do_not_encrypt,  // output_block <= input_block

  input wire [15:0][7:0] input_block,
  output logic [15:0][7:0] output_block
);

// From https://nvlpubs.nist.gov/nistpubs/FIPS/NIST.FIPS.197.pdf
// Cipher(byte in[4*Nb], byte out[4*Nb], word w[Nb*(Nr+1)])
// begin
// byte state[4,Nb]
// state = in
// AddRoundKey(state, w[0, Nb-1]) // See Sec. 5.1.4
// for round = 1 step 1 to Nr–1
//   SubBytes(state) // See Sec. 5.1.1
//   ShiftRows(state) // See Sec. 5.1.2
//   MixColumns(state) // See Sec. 5.1.3
//   AddRoundKey(state, w[round*Nb, (round+1)*Nb-1])
// end for
// SubBytes(state)
// ShiftRows(state)
// AddRoundKey(state, w[Nr*Nb, (Nr+1)*Nb-1])
// out = state
// end

function [15:0][7:0] ShiftRows (input [15:0][7:0] block);
  ShiftRows = {block[15], block[10], block[ 5], block[ 0],
               block[11], block[ 6], block[ 1], block[12],
               block[ 7], block[ 2], block[13], block[ 8],
               block[ 3], block[14], block[ 9], block[ 4]};
endfunction

// Functions for Mix columns
function [7:0] GM2(input [7:0] input_byte);
  begin
    GM2 = {input_byte[6:0], 1'b0} ^ (8'h1b & {8{input_byte[7]}});
  end
endfunction

function [7:0] GM3(input [7:0] input_byte);
  begin
    GM3 = GM2(input_byte) ^ input_byte;
  end
endfunction

function [3:0][7:0] MixWord(input [3:0][7:0] input_word);
  MixWord[3] = GM2(input_word[3]) ^ GM3(input_word[2]) ^ input_word[1] ^ input_word[0];
  MixWord[2] = input_word[3] ^ GM2(input_word[2]) ^ GM3(input_word[1]) ^ input_word[0];
  MixWord[1] = input_word[3] ^ input_word[2] ^ GM2(input_word[1]) ^ GM3(input_word[0]);
  MixWord[0] = GM3(input_word[3]) ^ input_word[2] ^ input_word[1] ^ GM2(input_word[0]);
endfunction

function [15:0][7:0] MixColumns(input [15:0][7:0] input_block);
  MixColumns = {MixWord(input_block[15:12]),
                MixWord(input_block[11:8]),
                MixWord(input_block[7:4]),
                MixWord(input_block[3:0])};
endfunction

logic [15:0][7:0] sbox_output;
logic [15:0][7:0] shiftrows_output;
logic [15:0][7:0] mixcolumns_output;
logic [15:0][7:0] addroundkey_output;

sbox #(.NumParallelBytes(16))
  sbox(
    .input_bytes(input_block),
    .output_bytes(sbox_output));

assign shiftrows_output = ShiftRows(sbox_output);
assign mixcolumns_output = MixColumns(shiftrows_output);
assign addroundkey_output = IsLastRound ? roundkey ^ shiftrows_output :
                                            roundkey ^ mixcolumns_output;

always_ff @(posedge clk) begin
  output_block <= do_not_encrypt ? input_block : addroundkey_output;
end

endmodule