# AES-GCM/GMAC implementation in verilog

This project contains several files that implement AES-GCM/GMAC in
SystemVerilog. We target a 100Gbps throughput on a sufficiently advanced FPGA.
To achieve this, we run four parallel 128 bit AES streams feeding a GMAC module.
The AES modules are fully pipelined and are capable of accepting one new data
word every cycle. Together, this design was synthesized on a Xilinx <> target
and was closed above 200MHz (hence 100Gbps).

At this time, it supports only 128 bit keys. It also assumes that Ek of GMAC
is AES(0).

The modules are organized as follows.

  *  Primitives

     *  gf_multiply_remainder.sv - Multiplies two N-bit numbers in 
     GF(parameterized dimension). It employs the remainder method to simplify
     the XOR tree. 

  *  AES

     *  sbox.sv - AES sbox. Hardcoded look up table.

     *  rcon.sv - Round constant generator. Very small circuit, may be optimized
     into hardcoded values if necessary.

     *  aes_roundkey_gen.sv - A combinational module that takes in the previous
     roundkey and generates the roundkey for one round. 

     *  aes_roundkey.sv - Runs a pipeline around a single instantiation of
     `aes_roundkey_gen` and saves roundkeys one cycle at a time for N cycles.
     N is the number of rounds for AES. The module latches the roundkey for each
     round in the first N cycles. Note that this module assumes that a packet is
     at least N cycles in length. If very short packets are to be encrypted,
     please pipeline this module by unrolling the aes_roundkey_gen
     instantiation. 

     *  aes_one_round.sv - Given an input_block and roundkey, does the
     transforms for one round. Flops the output and gives out output_block.

     *  aes_encrypt.sv - Puts these modules together to create a pipeline. Note
     that except for the keys, there is no state in this module. We expect
     `key_init` to go high for exactly one cycle with the `key`, after which
     for every `data_in`, a `data_out` appears after N cycles.

  *  GHASH - primarily built around the `gf_multiply_remainder` module.

     *  parallel_gmac.sv - Implements 4 parallel GHASH functions. For one cycle
     at the start of a packet, it takes in H and computes H^2, H^3 and H^4. It
     computes the GHASH in subsequent cycles.

     *  gf_mult_gmac_poly_remainders.sv - Precomputed values of remainders for
     computing gf_multiply using the remainder method.

  *  Stray files - Not really used in the pipeline, but in here for
     experimentatal purposes.

     *  gf_multiply_long_form.sv - GF multiplication implemented the "normal"
     way.

     *  xilinx_wrapper.sv - Just a dumb wrapper that helps instantiate modules
     with some connectivity around. Ensure that Xilinx does not synthesize out
     the design.
