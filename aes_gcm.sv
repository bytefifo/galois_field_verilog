/* Copyright 2019 Raghu Balasubramanian

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*/

// AES GCM
// data, auth_data are expected to be padded to 128 bytes.
module aes_gcm
  #(parameter KeyBytes=16,
    parameter aes_pkg::aes_type_t AesType = aes_pkg::aes_128)
  (
  input wire clk,
  input wire rst,

  input wire            aes256,
  input wire            data_in_valid,
  input aes_pkg::data_t data_in,
  // data_in is not consumed when this is high.
  output logic          data_in_hold,

  // word [0] is the first word.
  output aes_pkg::data_t data_out,
  output logic           data_out_valid,

  output logic           gmac_valid,
  output logic [127:0]   gmac
);

localparam NumStages = aes_pkg::NumStages(AesType);

// decode control signals
logic data_in_valid_and_not_hold;
logic key_init;
assign key_init = data_in_valid_and_not_hold == 1'b1 && data_in.whats_in_data == aes_pkg::key;

logic [3:0][127:0] iv_and_counter;
logic [3:0][127:0] encrypted_iv_and_counter;

logic [95:0] iv;
logic [31:0] counter;

logic        aes_initialize_cycle;
logic        ek_compute_cycle;
logic        h_power_compute_cycle;
logic        len_ghash_cycle;

assign data_in_valid_and_not_hold = data_in_valid & ~data_in_hold;

always_ff @(posedge clk) begin : always_iv
  if (data_in_valid_and_not_hold == 1'b1 &&
      data_in.whats_in_data == aes_pkg::iv)
    iv <= data_in.message[2:0];
end

always_ff @(posedge clk) begin: always_counter
  if (data_in_valid_and_not_hold == 1'b1) begin
    if (data_in.whats_in_data == aes_pkg::iv) begin
      counter <= 'd2;
    end
    else if (data_in.whats_in_data == aes_pkg::payload) begin
                           // valid words is smaller by one and in 32 bit words
      counter <= counter + ((data_in.valid_bytes + 'd15) >> 4);
    end
  end
end

always_comb begin: always_comb_iv_and_counter
  for (int idx = 0; idx < 4; idx++) begin
    if (ek_compute_cycle) begin
      iv_and_counter[idx] = 128'h0;
    end else if (data_in_valid_and_not_hold == 1'b1 &&
                 data_in.whats_in_data == aes_pkg::iv) begin
      iv_and_counter[idx] = {data_in.message[2:0], 32'd1};
    end else begin
      iv_and_counter[idx] = (iv << 32) + (counter + idx);
    end
  end
end

genvar aes_idx;
generate
  for (aes_idx = 0; aes_idx < 4; aes_idx++) begin
    aes_encrypt 
       #(.NumRounds(NumStages))
      aes_encrypt(
        .clk(clk),
        .rst(rst),
        .aes256(aes256),
        .key_init(key_init),  // same cycle for all 4 pipelines.
       // .key(data_in.message[0]),
        .key(data_in.message[0]),
        .key1(data_in.message[1]),
        .do_not_encrypt(1'b0),
        .data_in(iv_and_counter[aes_idx]),
        .data_out(encrypted_iv_and_counter[aes_idx]));
  end
endgenerate

// pipeline the data in parallel to the encryption engine.
// two extra cycles to help with h_valid
// We let AES initialize for one cycle, then push a zero for e_k_0.
aes_pkg::data_t data_in_pipeline[NumStages+2:0];
logic [NumStages+2:0] data_in_valid_pipeline;

assign data_in_pipeline[0]       = data_in;
assign data_in_valid_pipeline[0] = data_in_valid_and_not_hold;

always_ff @(posedge clk) begin
  for (int idx = 1; idx <= NumStages+2; idx++) begin
    data_in_valid_pipeline[idx] <= data_in_valid_pipeline[idx-1];
    data_in_pipeline[idx]       <= data_in_pipeline[idx-1];
  end
end

assign data_in_hold = ek_compute_cycle | aes_initialize_cycle |
                      h_power_compute_cycle | len_ghash_cycle;

always_ff @(posedge clk) begin
  aes_initialize_cycle  <= (data_in_valid_and_not_hold == 1'b1 &&
                            data_in.whats_in_data == aes_pkg::key);
  ek_compute_cycle      <= aes_initialize_cycle;
  h_power_compute_cycle <= (data_in_valid_and_not_hold == 1'b1 &&
                            data_in.whats_in_data == aes_pkg::iv);
  len_ghash_cycle       <= (data_in_valid_and_not_hold == 1'b1 && data_in.is_last == 1'b1);
end

// keep track of count for len(A) || len(C).
logic [63:0] count_auth_bits;
logic [63:0] count_payload_bits;

always_ff @(posedge clk) begin
  if (data_in_valid_pipeline[NumStages-1] == 1'b1) begin
    case (data_in_pipeline[NumStages-1].whats_in_data)
      aes_pkg::iv: begin
        count_auth_bits <= '0;
        count_payload_bits <= '0;
      end
      aes_pkg::auth: begin
        count_auth_bits <= count_auth_bits +
          ((data_in_pipeline[NumStages-1].valid_bytes + 'd1) << 3);  // * 8 (in bits)
      end
      aes_pkg::payload: begin
          count_payload_bits <= count_payload_bits +
          ((data_in_pipeline[NumStages-1].valid_bytes + 'd1) << 3);  // * 8 (in bits)
      end
    endcase
  end
end

logic [63:0][7:0] message_mask_helper;
logic [3:0][127:0] data_in_message_mask;

always_comb begin: always_comb_data_in_mask
  message_mask_helper = '1;

  for (int i = 0; i < 64; i++) begin
    if (i <= data_in_pipeline[NumStages-1].valid_bytes)
      message_mask_helper[63-i] = '1;
    else
      message_mask_helper[63-i] = '0;
  end
  // nasty endian convertion :(
  data_in_message_mask[0] = message_mask_helper[63:48];
  data_in_message_mask[1] = message_mask_helper[47:32];
  data_in_message_mask[2] = message_mask_helper[31:16];
  data_in_message_mask[3] = message_mask_helper[15:0];
end


logic [127:0] h;
logic         h_valid;
logic [3:0][127:0] parallel_gmac_message;
logic [1:0]   parallel_gmac_message_words;
logic         parallel_gmac_message_valid;
logic [127:0] parallel_gmac_output;
logic         parallel_gmac_output_valid;
logic         busy;

// H is EK(0).
assign h       = encrypted_iv_and_counter[0];
assign h_valid = (data_in_valid_pipeline[NumStages+1] == 1'b1 &&
                  data_in_pipeline[NumStages+1].whats_in_data == aes_pkg::key);

always_comb begin: always_comb_gmac_payload
  parallel_gmac_message_valid = 1'b0;
  parallel_gmac_message = '0;
  parallel_gmac_message_words = '0;
  if (data_in_valid_pipeline[NumStages] && data_in_pipeline[NumStages].is_last) begin
    parallel_gmac_message_valid = 1'b1;
    parallel_gmac_message[0] = {count_auth_bits, count_payload_bits};
    parallel_gmac_message_words = 'd4;
  end
  else if (data_in_valid_pipeline[NumStages-1] &&
      data_in_pipeline[NumStages-1].whats_in_data == aes_pkg::auth) begin
    parallel_gmac_message_valid = 1'b1;
    parallel_gmac_message = data_in_pipeline[NumStages-1].message & data_in_message_mask;
    parallel_gmac_message_words = (data_in_pipeline[NumStages-1].valid_bytes) >> 4;
  end
  else if (data_in_valid_pipeline[NumStages-1] &&
      data_in_pipeline[NumStages-1].whats_in_data == aes_pkg::payload) begin
    parallel_gmac_message_valid = 1'b1;
    parallel_gmac_message = (data_in_pipeline[NumStages-1].message ^ encrypted_iv_and_counter) & data_in_message_mask;
    parallel_gmac_message_words = (data_in_pipeline[NumStages-1].valid_bytes) >> 4;
  end
end

// GMAC instantiation
parallel_gmac
  parallel_gmac(
    .clk(clk),
    .rst(rst),
    .h(h),
    .h_valid(h_valid),
    .message(parallel_gmac_message),
    .message_words(parallel_gmac_message_words),
    .message_valid(parallel_gmac_message_valid),
    .gmac(parallel_gmac_output),
    .gmac_valid(parallel_gmac_output_valid),
    .busy(busy));

// When the {IV, 0^31, 1} comes through encrypted, save that.
logic [127:0] e_k_y0;
always_ff @(posedge clk) begin: always_save_ek_iv_1
  if (rst) begin
    e_k_y0 <= '0;
  end else begin
    if (data_in_valid_pipeline[NumStages-1] == 1'b1 &&
        data_in_pipeline[NumStages-1].whats_in_data == aes_pkg::iv)
      e_k_y0 <= encrypted_iv_and_counter[0];
  end
end

always_comb begin: always_comb_drive_gmac_out
  gmac_valid = 1'b0;
  gmac       = '0;
  // One cycle after IV, e_k_y0 is ready and that forms a perfectly valid
  // GMAC.
  if (data_in_valid_pipeline[NumStages] == 1'b1 &&
      data_in_pipeline[NumStages].whats_in_data == aes_pkg::iv &&
      data_in_pipeline[NumStages].is_last == 1'b1) begin
    gmac_valid = 1'b1;
    gmac       = e_k_y0;  // fresh off the latch.
  end
  else if (data_in_valid_pipeline[NumStages+2] == 1'b1 &&
           data_in_pipeline[NumStages+2].is_last == 1'b1) begin
    gmac_valid = !(data_in_pipeline[NumStages+2].whats_in_data == aes_pkg::key ||
                   data_in_pipeline[NumStages+2].whats_in_data == aes_pkg::iv);
    gmac       = parallel_gmac_output ^ e_k_y0;
  end
end

// Can shave one cycle here
always_ff @(posedge clk) begin: always_data_out
    data_out_valid <= 1'b0;
    data_out       <= '0;

  if (data_in_valid_pipeline[NumStages-1] == 1'b1) begin
    case (data_in_pipeline[NumStages-1].whats_in_data)
      aes_pkg::key : begin
        data_out_valid <= 1'b0;
        data_out       <= '0;
      end
      aes_pkg::iv : begin
        data_out_valid <= 1'b0;
        data_out       <= '0;
      end
      aes_pkg::auth : begin
        data_out_valid <= 1'b1;
        data_out          <= data_in_pipeline[NumStages-1];
        data_out.message  <= parallel_gmac_message;
      end
      aes_pkg::payload: begin
        data_out_valid <= 1'b1;
        data_out          <= data_in_pipeline[NumStages-1];
        data_out.message  <= parallel_gmac_message;
      end
      default : data_out_valid <= 1'b0;
    endcase
  end
end

/* This assert causes Vivado to freak out.
// assertions, static assertions are in falling edge of reset only.
// For now, only aes 128 is supported
always_ff @(negedge rst) begin : static_assertions
  assert (AesType == aes_pkg::aes_128) else $error("Only AES 128 is supported at present");
end

*/

endmodule
