/* Copyright 2019 Raghu Balasubramanian

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*/

// aes_roundkey generates N roundkeys for N rounds. It takes
// N cycles to fully populate the round keys.

module aes_roundkey
  #(parameter KeyBytes=16,
    parameter NumRounds=11)
  (
  input wire clk,
  input wire start,
  input wire aes256,
  input wire [KeyBytes-1:0][7:0] key,  // valid only when start is one.
  input wire [KeyBytes-1:0][7:0] key1,  // valid only when start is one.
  
  output logic [NumRounds-1:0][KeyBytes-1:0][7:0] roundkeys
);

logic [KeyBytes-1:0][7:0] roundkey;
logic [KeyBytes-1:0][7:0] previous_roundkey;
logic [$clog2(NumRounds+1)-1:0] round;
logic keygen_in_progress;
logic [7:0] rcon;
logic [3:0][7:0] rksel;
logic            is_g;
logic            rcon_act;

always_ff @(posedge clk) begin : always_round_counter
  if (start) begin
    round <= aes256 ? 'd2 : 'd1;
    keygen_in_progress <= 1'b1;
  end
  else begin
    round <= round + 'd1;

    if (round >= NumRounds)
      keygen_in_progress <= 1'b0;
  end
end : always_round_counter

always_ff @(posedge clk) begin : always_roundkey
  if (start) begin
    roundkeys[0] <= key;
    if(aes256) begin 
      roundkeys[1] <= key1;
       previous_roundkey <= key;
       rksel <= key1[3:0];
       is_g  <= 1'b0; 
       rcon_act <= 1'b0;
     end
    else begin
      previous_roundkey <= key;
      rksel <= key[3:0]; 
      is_g  <= 1'b0;
      rcon_act <= 1'b1;
    end   
  end       
 else if(aes256 && keygen_in_progress) begin
      previous_roundkey <= roundkeys[round-1];
      rksel <= roundkey[3:0]; 
      roundkeys[round] <= roundkey; 
      is_g  <= !is_g;         
      rcon_act <= !is_g;
 end else begin             
     if (keygen_in_progress) 
      roundkeys[round] <= roundkey;
      previous_roundkey <= roundkey;
      rksel <= roundkey[3:0]; 
      is_g  <= 1'b0; 
      rcon_act <= 1'b1;
    end

end

aes_roundkey_gen #(.KeyBytes(KeyBytes))
  aes_roundkey_gen(
    .previous_roundkey(previous_roundkey),
    .rksel(rksel),
    .is_g(is_g),
    .rcon(rcon),
    .roundkey(roundkey));  // flopped inside.

rcon
  rcon_inst(
    .clk(clk),
    .act(rcon_act),
    .start(start),
    .rcon(rcon));

endmodule
