/* Copyright 2019 Raghu Balasubramanian

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*/

module aes_gcm_test ();

logic clk;
logic rst;
logic           data_in_valid;
aes_pkg::data_t data_in;
logic           data_in_hold;
aes_pkg::data_t data_out;
logic           data_out_valid;
logic           gmac_valid;
logic [127:0]   gmac;

initial begin
  rst = 1'b1;
  rst = #60 1'b0;
end

initial begin
  clk = 1'b0;
  forever begin
    clk = #15 ~clk;
  end
end

aes_gcm // default parameters
  aes_gcm(
    .clk(clk),
    .rst(rst),
    .data_in_valid(data_in_valid),
    .data_in(data_in),
    .data_in_hold(data_in_hold),
    .data_out(data_out),
    .data_out_valid(data_out_valid),
    .gmac_valid(gmac_valid),
    .gmac(gmac));

  function logic [7:0][7:0] le2be(logic [7:0][7:0] data_in);
    for (int idx = 0; idx < 8; idx++) begin
      le2be[7-idx] = data_in[idx];
    end
    return le2be;
  endfunction
/*
// packet is one IV followed by some auth data and message data.
typedef struct packed{
  logic [95:0] iv;
  logic [127:0] auth_data[$];
  logic [127:0] message_data[$];
} one_packet_t;

// A stream is a number of packets which are encrypted with a single key.
typedef struct packed{
  logic [127:0] key;
  one_packet_t packet[$];
} one_stream_t;

  logic is_last;
  whats_in_data_t whats_in_data;
  // 0 => 3 words valid.
  // must be 1 when key.
  logic [1:0] valid_bytes;
  // word 0 is the first word.
  // If 2 words are valid, that's words 0 and 1.
  // key must be in message[0]
  logic [3:0][127:0] message;
*/

aes_pkg::data_t input_data_q[$];
aes_pkg::data_t one_input_data;

logic [127:0] expected_gmac_q[$];
logic [511:0] expected_data_out_q[$];

initial begin
  // test cases from
  // http://luca-giuzzi.unibs.it/corsi/Support/papers-cryptography/gcm-spec.pdf
  // Page 27.

  // This is an all zeros no Paylaod test.
  // one_input_data.is_last = 1'b0;
  // key cycle.
  one_input_data.whats_in_data = aes_pkg::key;
  one_input_data.message[0] = '0;
  one_input_data.valid_bytes = 15;
  input_data_q.push_back(one_input_data);
  // iv stage.
  one_input_data.whats_in_data = aes_pkg::iv;
  one_input_data.message[0] = '0;
  one_input_data.valid_bytes = 11;
  one_input_data.is_last = 1'b1;
  input_data_q.push_back(one_input_data);
  // stop here for this one.
  expected_gmac_q.push_back(128'h58e2fccefa7e3061367f1d57a4e7455a);

  // This is an all zeros, one payload with 16 bytes test.
  // new test, same Key, IV, but this time with payload.
  one_input_data.whats_in_data = aes_pkg::key;
  one_input_data.message[0] = '0;
  one_input_data.valid_bytes = 15;
  one_input_data.is_last = 1'b0;
  input_data_q.push_back(one_input_data);
  // iv stage.
  one_input_data.whats_in_data = aes_pkg::iv;
  one_input_data.message[0] = '0;
  one_input_data.valid_bytes = 11;
  one_input_data.is_last = 1'b0;
  input_data_q.push_back(one_input_data);
  // payload
  one_input_data.whats_in_data = aes_pkg::payload;
  one_input_data.message[0] = '0;
  one_input_data.valid_bytes = 15;
  one_input_data.is_last = 1'b1;
  input_data_q.push_back(one_input_data);
  expected_data_out_q.push_back({
    128'h0, 128'h0, 128'h0, 128'h0388dace60b6a392f328c2b971b2fe78});
  expected_gmac_q.push_back(128'hab6e47d42cec13bdf53a67b21257bddf);

  // Incremental key test
  // NOTE: THE EXPECTED DATA IN THIS TEST ARE REVERSE ENGINEERED.
  one_input_data.whats_in_data = aes_pkg::key;
  one_input_data.message[0] = 128'h000102030405060708090a0b0c0d0e0f;
  one_input_data.valid_bytes = 15;
  one_input_data.is_last = 1'b0;
  input_data_q.push_back(one_input_data);
  // iv stage.
  one_input_data.whats_in_data = aes_pkg::iv;
  one_input_data.message[0] = '0;
  one_input_data.valid_bytes = 11;
  one_input_data.is_last = 1'b0;
  input_data_q.push_back(one_input_data);
  // payload
  one_input_data.whats_in_data = aes_pkg::payload;
  one_input_data.message[0] = 128'h000102030405060708090a0b0c0d0e0f;
  one_input_data.message[1] = 128'h101112131415161718191a1b1c1d1e1f;
  one_input_data.message[2] = 128'h202122232425262728292a2b2c2d2e2f;
  one_input_data.message[3] = 128'h303132333435363738393a3b3c3d3e3f;
  one_input_data.valid_bytes = 63;
  one_input_data.is_last = 1'b0;
  input_data_q.push_back(one_input_data);
  expected_data_out_q.push_back({
    128'habb3abba5047b776786719e6a320ee24,
    128'h104294fc2e09fd97ad3b7bf9ea44ff90,
    128'ha9bc393d207fd42f48442c4580aae249,
    128'h49d785509d9ea08beb8070636c8cbe92});
  one_input_data.whats_in_data = aes_pkg::payload;
  one_input_data.message[0] = 128'h404142434445464748494a4b4c4d4e4f;
  one_input_data.message[1] = 128'h505152535455565758595a5b5c5d5e5f;
  one_input_data.message[2] = 128'h606162636465666768696a6b6c6d6e6f;
  one_input_data.message[3] = 128'h707172737475767778797a7b7c7d7e7f;
  one_input_data.valid_bytes = 63;
  one_input_data.is_last = 1'b1;
  input_data_q.push_back(one_input_data);
  expected_data_out_q.push_back({
    128'h113316296b4655290f95758096496877,
    128'hc545a50e9d2abbff9fbf3f66bcd424fc,
    128'he9637d4a927e6ebee6817188ba264d46,
    128'h941fbe112cece9ad899b63aced0f582d});
  // TODO: Reverse engineered, check.
  expected_gmac_q.push_back(128'he6820e76dd80be537b2acd29b7b7e422);

  // Test case with Authentication and non multiple of 4 bytes.
  // Key, IV 0, Augh 13 bytes of 0 and Payload 64 bytes of zeros.
  one_input_data.whats_in_data = aes_pkg::key;
  one_input_data.message[0] = '0;
  one_input_data.valid_bytes = 15;
  one_input_data.is_last = 1'b0;
  input_data_q.push_back(one_input_data);
  // iv stage.
  one_input_data.whats_in_data = aes_pkg::iv;
  one_input_data.message[0] = '0;
  one_input_data.valid_bytes = 11;
  one_input_data.is_last = 1'b0;
  input_data_q.push_back(one_input_data);
  // 13 bytes of auth
  one_input_data.whats_in_data = aes_pkg::auth;
  one_input_data.message[0] = '0;
  one_input_data.message[1] = 128'hx;
  one_input_data.message[2] = 128'hx;
  one_input_data.message[3] = 128'hx;
  one_input_data.valid_bytes = 12;  // <== NOTE
  one_input_data.is_last = 1'b0;
  input_data_q.push_back(one_input_data);
  expected_data_out_q.push_back({128'h0, 128'h0, 128'h0, 128'h0});

  // 64 bytes of payload
  one_input_data.whats_in_data = aes_pkg::payload;
  one_input_data.message = '0;
  one_input_data.valid_bytes = 63;  // <== NOTE
  one_input_data.is_last = 1'b1;
  input_data_q.push_back(one_input_data);
  expected_data_out_q.push_back({
    128'hc94da219118e297d7b7ebcbcc9c388f2,
    128'h200211214e7394da2089b6acd093abe0,
    128'hf795aaab494b5923f7fd89ff948bc1e0,
    128'h0388dace60b6a392f328c2b971b2fe78});

  expected_gmac_q.push_back(128'h66165d39739c50c90727e7d49127146b);

  // Key 16 B, IV 12, Auth 13, Payload 65, random.
  one_input_data.whats_in_data = aes_pkg::key;
  one_input_data.message[0] = {le2be(64'he1781ef4d81b855b), le2be(64'h4042dbac5982a78e)};
  one_input_data.valid_bytes = 15;
  one_input_data.is_last = 1'b0;
  input_data_q.push_back(one_input_data);

  one_input_data.whats_in_data = aes_pkg::iv;
  one_input_data.message[0] = {le2be(64'h1e2a8217e9bff115), le2be(64'h000000002b28b48c)} >> 32;
  one_input_data.valid_bytes = 11;
  one_input_data.is_last = 1'b0;
  input_data_q.push_back(one_input_data);

  one_input_data.whats_in_data = aes_pkg::auth;
  one_input_data.message[0] = {le2be(64'hd9c54e3c1479ae4c), le2be(64'h000000bc161790a4)};
  one_input_data.message[1] = 128'hx;
  one_input_data.message[2] = 128'hx;
  one_input_data.message[3] = 128'hx;
  one_input_data.valid_bytes = 12;  // <== NOTE
  one_input_data.is_last = 1'b0;
  input_data_q.push_back(one_input_data);
  // same as input (aut data)
  expected_data_out_q.push_back({128'h0, 128'h0, 128'h0,
    {le2be(64'hd9c54e3c1479ae4c), le2be(64'h000000bc161790a4)}});

  one_input_data.whats_in_data = aes_pkg::payload;
  one_input_data.message[0] = {le2be(64'h0ba017aa8da76afd), le2be(64'h9ddd9fa240516c55)};
  one_input_data.message[1] = {le2be(64'h03274127e141d81c), le2be(64'ha8164d5a21aca1c8)};
  one_input_data.message[2] = {le2be(64'h39ff9494d4e1383a), le2be(64'ha08850b963457725)};
  one_input_data.message[3] = {le2be(64'had55dca921750fa0), le2be(64'h5c0792d03d1ab88b)};
  one_input_data.valid_bytes = 63;
  one_input_data.is_last = 1'b0;
  input_data_q.push_back(one_input_data);
  expected_data_out_q.push_back({
    le2be(64'hd428db4233ba9d31), le2be(64'h4ba9db3b24cd8aba),
    le2be(64'h8750a29a91bc5b38), le2be(64'hb01901af38f84e49),
    le2be(64'h8e891ac4657cfad6), le2be(64'h70de16667c06cb2d),
    le2be(64'h6fd31058cb968a3b), le2be(64'hd91af0012f531e36)});

  one_input_data.whats_in_data = aes_pkg::payload;
  one_input_data.message[0] = {le2be(64'h01), 64'h0};
  one_input_data.message[1] = 128'hx;
  one_input_data.message[2] = 128'hx;
  one_input_data.message[3] = 128'hx;
  one_input_data.valid_bytes = 0;
  one_input_data.is_last = 1'b1;
  input_data_q.push_back(one_input_data);
  expected_data_out_q.push_back({128'h0, 128'h0, 128'h0, le2be(64'h3c), 64'h0});

  expected_gmac_q.push_back({le2be(64'h046a5fc20c514a28), le2be(64'h973b380805ba6194)});
end

// Give the pipeline a 12 cycle if the previous packet ends too soon.
logic wait_12_cycles;
integer twelve_cycle_counter;

always_ff @(posedge clk) begin
  if (rst) begin
    twelve_cycle_counter <= 0;
    wait_12_cycles       <= 1'b0;
  end
  else begin
    if (twelve_cycle_counter != 0) begin
      twelve_cycle_counter <= twelve_cycle_counter - 1;
    end
    else if (data_in_valid == 1'b1 &&
             data_in.whats_in_data == aes_pkg::key) begin
      twelve_cycle_counter <= 15;
    end

    if (twelve_cycle_counter > 0 && data_in_valid == 1'b1 &&
        data_in_hold == 1'b0 && data_in.is_last == 1'b1) begin
      wait_12_cycles <= 1'b1;
    end
    else if (twelve_cycle_counter == 0) begin
      wait_12_cycles <= 1'b0;
    end
  end
end

always_ff @(posedge clk) begin
  if (rst) begin
    data_in <= '0;
    data_in_valid <= 1'b0;
  end else begin
    if (twelve_cycle_counter > 0 && data_in_valid == 1'b1 &&
        data_in_hold == 1'b0 && data_in.is_last == 1'b1) begin
      data_in_valid <= 1'b0;
    end
    else if (wait_12_cycles) begin
      data_in_valid <= 1'b0;
    end
    else if (~data_in_hold) begin
      if (input_data_q.size() > 0) begin
        data_in <= input_data_q.pop_front();
        data_in_valid <= 1'b1;
      end
      else begin
        data_in <= '0;
        data_in_valid <= 1'b0;
      end
    end
  end
end

// AES output check

logic [511:0] expected_data_out_q_top;

always_ff @(posedge clk) begin
  if (~rst) begin
    expected_data_out_q_top = expected_data_out_q[0];
    if (data_out_valid) begin
      if (expected_data_out_q.size() <= 0) begin
        $error("Expected data_out queue empty");
        $finish();
      end
      if (data_out.message !== expected_data_out_q_top) begin
        $display(" Expected:\n128'h%x\n128'h%x\n128'h%x\n128'h%x\n",
                 expected_data_out_q_top[127:0],
                 expected_data_out_q_top[255:128],
                 expected_data_out_q_top[383:256],
                 expected_data_out_q_top[511:384]);
        $display(" Got:\n128'h%x\n128'h%x\n128'h%x\n128'h%x\n",
                 data_out.message[0],
                 data_out.message[1],
                 data_out.message[2],
                 data_out.message[3]);
        $error("Expected data out does not match actual.");
        $finish();
      end else begin
        expected_data_out_q.pop_front();
        $display("AES match");
      end
    end
  end
end

logic [127:0] expected_gmac_q_top;

always_ff @(posedge clk) begin
  if (~rst) begin
    if (gmac_valid) begin
      expected_gmac_q_top = expected_gmac_q[0];
      if (expected_gmac_q.size() <= 0) begin
        $error("Expected GMAC queue empty");
        $finish();
      end
      if (gmac !== expected_gmac_q_top) begin
        $error("Expected GMAC did not match actual GMAC:\nGot: 128'h%x\nExp: 128'h%x",
               gmac, expected_gmac_q_top);
        $finish();
      end else begin
        expected_gmac_q.pop_front();
        $display("GMAC Match");
      end
    end

    if (expected_gmac_q.size() == 0) begin
      $display("Test passed");
      $finish();
    end // if (expected_gmac_q.size() == 0)
  end
end

endmodule