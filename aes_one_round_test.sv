/* Copyright 2019 Raghu Balasubramanian

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*/

module aes_one_round_test ();
parameter KeyBytes=16;

logic clk;
logic rst;

logic [KeyBytes-1:0][7:0] roundkey;
logic [15:0][7:0] input_block;
logic [15:0][7:0] output_block;
logic [15:0][7:0] expected_output;

initial begin
  clk = 1'b0;
  forever begin
    clk = #15 ~clk;
  end
end

initial begin
  rst = 1'b1;
  rst = #60 1'b0;
end

aes_one_round
  #(.IsLastRound(0),
    .KeyBytes(KeyBytes))
  aes_one_round(
    .clk(clk),
    .roundkey(roundkey),
    .input_block(input_block),
    .output_block(output_block));

// Let's do round 2 of NIST example.
// Skip the first round which is input ^ key;
assign input_block = 128'h3243f6a8_885a308d_313198a2_e0370734 ^
                     128'h2b7e1516_28aed2a6_abf71588_09cf4f3c;
assign roundkey = 128'ha0fafe17_88542cb1_23a33939_2a6c7605;
assign expected_output = 128'ha49c7ff2_689f352b_6b5bea43_026a5049;

always_ff @(posedge clk) begin
  if (~rst) begin
    if (output_block !== expected_output) begin
      $display("Expected : %x \nGot: %x", expected_output, output_block);
      $display("Test failed");
    end
  end
end

endmodule