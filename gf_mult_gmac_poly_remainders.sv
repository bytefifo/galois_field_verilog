/* Copyright 2019 Raghu Balasubramanian

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*/

// Hardcoded remainders for GF128 with the primitive polynomial
//

module gf_mult_gmac_poly_remainders (
  output logic [2*128-2:0][127:0] remainders
);

assign remainders[0] = 'h1;
assign remainders[1] = 'h2;
assign remainders[2] = 'h4;
assign remainders[3] = 'h8;
assign remainders[4] = 'h10;
assign remainders[5] = 'h20;
assign remainders[6] = 'h40;
assign remainders[7] = 'h80;
assign remainders[8] = 'h100;
assign remainders[9] = 'h200;
assign remainders[10] = 'h400;
assign remainders[11] = 'h800;
assign remainders[12] = 'h1000;
assign remainders[13] = 'h2000;
assign remainders[14] = 'h4000;
assign remainders[15] = 'h8000;
assign remainders[16] = 'h10000;
assign remainders[17] = 'h20000;
assign remainders[18] = 'h40000;
assign remainders[19] = 'h80000;
assign remainders[20] = 'h100000;
assign remainders[21] = 'h200000;
assign remainders[22] = 'h400000;
assign remainders[23] = 'h800000;
assign remainders[24] = 'h1000000;
assign remainders[25] = 'h2000000;
assign remainders[26] = 'h4000000;
assign remainders[27] = 'h8000000;
assign remainders[28] = 'h10000000;
assign remainders[29] = 'h20000000;
assign remainders[30] = 'h40000000;
assign remainders[31] = 'h80000000;
assign remainders[32] = 'h100000000;
assign remainders[33] = 'h200000000;
assign remainders[34] = 'h400000000;
assign remainders[35] = 'h800000000;
assign remainders[36] = 'h1000000000;
assign remainders[37] = 'h2000000000;
assign remainders[38] = 'h4000000000;
assign remainders[39] = 'h8000000000;
assign remainders[40] = 'h10000000000;
assign remainders[41] = 'h20000000000;
assign remainders[42] = 'h40000000000;
assign remainders[43] = 'h80000000000;
assign remainders[44] = 'h100000000000;
assign remainders[45] = 'h200000000000;
assign remainders[46] = 'h400000000000;
assign remainders[47] = 'h800000000000;
assign remainders[48] = 'h1000000000000;
assign remainders[49] = 'h2000000000000;
assign remainders[50] = 'h4000000000000;
assign remainders[51] = 'h8000000000000;
assign remainders[52] = 'h10000000000000;
assign remainders[53] = 'h20000000000000;
assign remainders[54] = 'h40000000000000;
assign remainders[55] = 'h80000000000000;
assign remainders[56] = 'h100000000000000;
assign remainders[57] = 'h200000000000000;
assign remainders[58] = 'h400000000000000;
assign remainders[59] = 'h800000000000000;
assign remainders[60] = 'h1000000000000000;
assign remainders[61] = 'h2000000000000000;
assign remainders[62] = 'h4000000000000000;
assign remainders[63] = 'h8000000000000000;
assign remainders[64] = 'h10000000000000000;
assign remainders[65] = 'h20000000000000000;
assign remainders[66] = 'h40000000000000000;
assign remainders[67] = 'h80000000000000000;
assign remainders[68] = 'h100000000000000000;
assign remainders[69] = 'h200000000000000000;
assign remainders[70] = 'h400000000000000000;
assign remainders[71] = 'h800000000000000000;
assign remainders[72] = 'h1000000000000000000;
assign remainders[73] = 'h2000000000000000000;
assign remainders[74] = 'h4000000000000000000;
assign remainders[75] = 'h8000000000000000000;
assign remainders[76] = 'h10000000000000000000;
assign remainders[77] = 'h20000000000000000000;
assign remainders[78] = 'h40000000000000000000;
assign remainders[79] = 'h80000000000000000000;
assign remainders[80] = 'h100000000000000000000;
assign remainders[81] = 'h200000000000000000000;
assign remainders[82] = 'h400000000000000000000;
assign remainders[83] = 'h800000000000000000000;
assign remainders[84] = 'h1000000000000000000000;
assign remainders[85] = 'h2000000000000000000000;
assign remainders[86] = 'h4000000000000000000000;
assign remainders[87] = 'h8000000000000000000000;
assign remainders[88] = 'h10000000000000000000000;
assign remainders[89] = 'h20000000000000000000000;
assign remainders[90] = 'h40000000000000000000000;
assign remainders[91] = 'h80000000000000000000000;
assign remainders[92] = 'h100000000000000000000000;
assign remainders[93] = 'h200000000000000000000000;
assign remainders[94] = 'h400000000000000000000000;
assign remainders[95] = 'h800000000000000000000000;
assign remainders[96] = 'h1000000000000000000000000;
assign remainders[97] = 'h2000000000000000000000000;
assign remainders[98] = 'h4000000000000000000000000;
assign remainders[99] = 'h8000000000000000000000000;
assign remainders[100] = 'h10000000000000000000000000;
assign remainders[101] = 'h20000000000000000000000000;
assign remainders[102] = 'h40000000000000000000000000;
assign remainders[103] = 'h80000000000000000000000000;
assign remainders[104] = 'h100000000000000000000000000;
assign remainders[105] = 'h200000000000000000000000000;
assign remainders[106] = 'h400000000000000000000000000;
assign remainders[107] = 'h800000000000000000000000000;
assign remainders[108] = 'h1000000000000000000000000000;
assign remainders[109] = 'h2000000000000000000000000000;
assign remainders[110] = 'h4000000000000000000000000000;
assign remainders[111] = 'h8000000000000000000000000000;
assign remainders[112] = 'h10000000000000000000000000000;
assign remainders[113] = 'h20000000000000000000000000000;
assign remainders[114] = 'h40000000000000000000000000000;
assign remainders[115] = 'h80000000000000000000000000000;
assign remainders[116] = 'h100000000000000000000000000000;
assign remainders[117] = 'h200000000000000000000000000000;
assign remainders[118] = 'h400000000000000000000000000000;
assign remainders[119] = 'h800000000000000000000000000000;
assign remainders[120] = 'h1000000000000000000000000000000;
assign remainders[121] = 'h2000000000000000000000000000000;
assign remainders[122] = 'h4000000000000000000000000000000;
assign remainders[123] = 'h8000000000000000000000000000000;
assign remainders[124] = 'h10000000000000000000000000000000;
assign remainders[125] = 'h20000000000000000000000000000000;
assign remainders[126] = 'h40000000000000000000000000000000;
assign remainders[127] = 'h80000000000000000000000000000000;
assign remainders[128] = 'h87;
assign remainders[129] = 'h10e;
assign remainders[130] = 'h21c;
assign remainders[131] = 'h438;
assign remainders[132] = 'h870;
assign remainders[133] = 'h10e0;
assign remainders[134] = 'h21c0;
assign remainders[135] = 'h4380;
assign remainders[136] = 'h8700;
assign remainders[137] = 'h10e00;
assign remainders[138] = 'h21c00;
assign remainders[139] = 'h43800;
assign remainders[140] = 'h87000;
assign remainders[141] = 'h10e000;
assign remainders[142] = 'h21c000;
assign remainders[143] = 'h438000;
assign remainders[144] = 'h870000;
assign remainders[145] = 'h10e0000;
assign remainders[146] = 'h21c0000;
assign remainders[147] = 'h4380000;
assign remainders[148] = 'h8700000;
assign remainders[149] = 'h10e00000;
assign remainders[150] = 'h21c00000;
assign remainders[151] = 'h43800000;
assign remainders[152] = 'h87000000;
assign remainders[153] = 'h10e000000;
assign remainders[154] = 'h21c000000;
assign remainders[155] = 'h438000000;
assign remainders[156] = 'h870000000;
assign remainders[157] = 'h10e0000000;
assign remainders[158] = 'h21c0000000;
assign remainders[159] = 'h4380000000;
assign remainders[160] = 'h8700000000;
assign remainders[161] = 'h10e00000000;
assign remainders[162] = 'h21c00000000;
assign remainders[163] = 'h43800000000;
assign remainders[164] = 'h87000000000;
assign remainders[165] = 'h10e000000000;
assign remainders[166] = 'h21c000000000;
assign remainders[167] = 'h438000000000;
assign remainders[168] = 'h870000000000;
assign remainders[169] = 'h10e0000000000;
assign remainders[170] = 'h21c0000000000;
assign remainders[171] = 'h4380000000000;
assign remainders[172] = 'h8700000000000;
assign remainders[173] = 'h10e00000000000;
assign remainders[174] = 'h21c00000000000;
assign remainders[175] = 'h43800000000000;
assign remainders[176] = 'h87000000000000;
assign remainders[177] = 'h10e000000000000;
assign remainders[178] = 'h21c000000000000;
assign remainders[179] = 'h438000000000000;
assign remainders[180] = 'h870000000000000;
assign remainders[181] = 'h10e0000000000000;
assign remainders[182] = 'h21c0000000000000;
assign remainders[183] = 'h4380000000000000;
assign remainders[184] = 'h8700000000000000;
assign remainders[185] = 'h10e00000000000000;
assign remainders[186] = 'h21c00000000000000;
assign remainders[187] = 'h43800000000000000;
assign remainders[188] = 'h87000000000000000;
assign remainders[189] = 'h10e000000000000000;
assign remainders[190] = 'h21c000000000000000;
assign remainders[191] = 'h438000000000000000;
assign remainders[192] = 'h870000000000000000;
assign remainders[193] = 'h10e0000000000000000;
assign remainders[194] = 'h21c0000000000000000;
assign remainders[195] = 'h4380000000000000000;
assign remainders[196] = 'h8700000000000000000;
assign remainders[197] = 'h10e00000000000000000;
assign remainders[198] = 'h21c00000000000000000;
assign remainders[199] = 'h43800000000000000000;
assign remainders[200] = 'h87000000000000000000;
assign remainders[201] = 'h10e000000000000000000;
assign remainders[202] = 'h21c000000000000000000;
assign remainders[203] = 'h438000000000000000000;
assign remainders[204] = 'h870000000000000000000;
assign remainders[205] = 'h10e0000000000000000000;
assign remainders[206] = 'h21c0000000000000000000;
assign remainders[207] = 'h4380000000000000000000;
assign remainders[208] = 'h8700000000000000000000;
assign remainders[209] = 'h10e00000000000000000000;
assign remainders[210] = 'h21c00000000000000000000;
assign remainders[211] = 'h43800000000000000000000;
assign remainders[212] = 'h87000000000000000000000;
assign remainders[213] = 'h10e000000000000000000000;
assign remainders[214] = 'h21c000000000000000000000;
assign remainders[215] = 'h438000000000000000000000;
assign remainders[216] = 'h870000000000000000000000;
assign remainders[217] = 'h10e0000000000000000000000;
assign remainders[218] = 'h21c0000000000000000000000;
assign remainders[219] = 'h4380000000000000000000000;
assign remainders[220] = 'h8700000000000000000000000;
assign remainders[221] = 'h10e00000000000000000000000;
assign remainders[222] = 'h21c00000000000000000000000;
assign remainders[223] = 'h43800000000000000000000000;
assign remainders[224] = 'h87000000000000000000000000;
assign remainders[225] = 'h10e000000000000000000000000;
assign remainders[226] = 'h21c000000000000000000000000;
assign remainders[227] = 'h438000000000000000000000000;
assign remainders[228] = 'h870000000000000000000000000;
assign remainders[229] = 'h10e0000000000000000000000000;
assign remainders[230] = 'h21c0000000000000000000000000;
assign remainders[231] = 'h4380000000000000000000000000;
assign remainders[232] = 'h8700000000000000000000000000;
assign remainders[233] = 'h10e00000000000000000000000000;
assign remainders[234] = 'h21c00000000000000000000000000;
assign remainders[235] = 'h43800000000000000000000000000;
assign remainders[236] = 'h87000000000000000000000000000;
assign remainders[237] = 'h10e000000000000000000000000000;
assign remainders[238] = 'h21c000000000000000000000000000;
assign remainders[239] = 'h438000000000000000000000000000;
assign remainders[240] = 'h870000000000000000000000000000;
assign remainders[241] = 'h10e0000000000000000000000000000;
assign remainders[242] = 'h21c0000000000000000000000000000;
assign remainders[243] = 'h4380000000000000000000000000000;
assign remainders[244] = 'h8700000000000000000000000000000;
assign remainders[245] = 'h10e00000000000000000000000000000;
assign remainders[246] = 'h21c00000000000000000000000000000;
assign remainders[247] = 'h43800000000000000000000000000000;
assign remainders[248] = 'h87000000000000000000000000000000;
assign remainders[249] = 'he000000000000000000000000000087;
assign remainders[250] = 'h1c00000000000000000000000000010e;
assign remainders[251] = 'h3800000000000000000000000000021c;
assign remainders[252] = 'h70000000000000000000000000000438;
assign remainders[253] = 'he0000000000000000000000000000870;
assign remainders[254] = 'hc0000000000000000000000000001067;

endmodule