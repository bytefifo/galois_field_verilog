/* Copyright 2019 Raghu Balasubramanian

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*/

// AES encryption
// This module encrypts blocks of 128 bits in consecutive cycles.
// The implementation is fully pipelined, and hence quite expensive.

// Protocol:
// key_init == 1'b1 initiates the first cycle of a new encryption cycle.
// Data provided in the subsequent cycles is ready N cycles later, where
// N = TODO for AES_128.

// Data and key is assumed to little endian. ie.,
// NIST example key : 128'h2b7e1516_28aed2a6_abf71588_09cf4f3c
//                  => Byte 15 = 0x2b, byte 0 = 0x3c.

// do_not_encrypt passes the data through the pipeline without modifying it.

module aes_encrypt
  #(parameter KeyBytes=16,
    parameter NumRounds = 11)
  (
  input wire clk,
  input wire rst,

  input wire aes256,
  input wire key_init,
  input wire [KeyBytes-1:0][7:0] key,  
  input wire [KeyBytes-1:0][7:0] key1,  

  input wire do_not_encrypt,
  input wire [15:0][7:0] data_in,
  output logic [15:0][7:0] data_out
);

//localparam NumRounds = 11;  // TODO upgrade to AES_192 and 256 later.

logic [NumRounds-1:0][KeyBytes-1:0][7:0] roundkeys;

aes_roundkey #(
    //.KeyBytes(KeyBytes),
    .NumRounds(NumRounds))
  aes_roundkey(
    .clk(clk),
    .start(key_init),
    .aes256(aes256),
    .key(key),
    .key1(key1),
    .roundkeys(roundkeys));

logic [NumRounds-1:0][15:0][7:0] intermediate_block;
logic [NumRounds-1:0] do_not_encrypt_p;

// Round 1.
assign intermediate_block[0] = data_in ^ roundkeys[0];
assign do_not_encrypt_p[0] = do_not_encrypt;

genvar gen_idx;
generate
  for (gen_idx = 1; gen_idx < NumRounds; gen_idx++) begin
    aes_one_round
      #(.IsLastRound(NumRounds-1==gen_idx),
        .KeyBytes(KeyBytes))
      aes_one_round(
        .clk(clk),
        .roundkey(roundkeys[gen_idx]),
        .do_not_encrypt(do_not_encrypt_p[gen_idx-1]),
        .input_block(intermediate_block[gen_idx-1]),
        .output_block(intermediate_block[gen_idx]));

    always_ff @(posedge clk) begin : always_do_not_encrypt
      if (rst) begin
        do_not_encrypt_p[gen_idx] <= 1'b0;
      end else begin
        do_not_encrypt_p[gen_idx] <= do_not_encrypt_p[gen_idx-1];
      end
    end
  end
endgenerate

assign data_out = intermediate_block[NumRounds-1];

endmodule
