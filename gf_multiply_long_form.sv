/* Copyright 2019 Raghu Balasubramanian

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*/

`timescale 1ns / 1ps

// Long form multiplication of two numbers in GF(2^dimension).
// This module is fully combinational (for now, may consider pipelining/flopping if we see timing
// issues).

module gf_multiply_long_form
  #(parameter integer Dimension = 128)
  (
    input wire clk,
    input wire rst,

    input wire [Dimension-1:0] primitive_poly,  // bit 2^Dimension == 1 is assumed.

    input wire [Dimension-1:0] operand_x,
    input wire [Dimension-1:0] operand_y,

    output logic [Dimension-1:0] result
  );

logic [Dimension-1:0][Dimension-1:0] operand_y_shifted;  // and folded in GF.

always_comb begin: always_comb_compute_y_shifted
  operand_y_shifted[0] = operand_y;
  for (int idx = 1; idx < Dimension; idx++) begin
    if (operand_y_shifted[idx-1][Dimension-1] == 1'b1)
      operand_y_shifted[idx] = (operand_y_shifted[idx-1] << 1) ^ primitive_poly;
    else
      operand_y_shifted[idx] = (operand_y_shifted[idx-1] << 1);
  end
end: always_comb_compute_y_shifted

always_comb begin: always_comb_result_compute
  result = 0;
  for (int idx = 0; idx < Dimension; idx++) begin
      result ^= (operand_x[idx] == 1'b1) ? operand_y_shifted[idx] : '0;
  end
end: always_comb_result_compute

endmodule