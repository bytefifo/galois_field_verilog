/* Copyright 2019 Raghu Balasubramanian

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*/

// GMAC of 4 128 bit inputs in one cycle.
// Load H once, in the subsequent cycles, push 4 inputs at a time.

module parallel_gmac (
  input wire clk,
  input wire rst,

  input wire [127:0] h,
  input wire         h_valid,

  input wire [3:0][127:0] message,
  // 0 => 1 word is valid, 1 => 2 words are valid...
  input wire [1:0]   message_words,
  input wire         message_valid,

  output logic [127:0] gmac,
  output logic         gmac_valid,

  output logic         busy  // will be high for 1 cycle after h_valid.
);
//timing fix
   logic [127:0] h_s;
   logic         h_valid_s;

   logic [3:0][127:0] message_s;
  // 0 => 1 word is valid, 1 => 2 words are valid...
   logic [1:0]   message_words_s;
   logic         message_valid_s;
  
always_ff @(posedge clk) begin
  if (rst) begin
    h_valid_s <= '0;
    message_valid_s <= '0;
  end else begin
    h_valid_s <= h_valid;
    message_valid_s <= message_valid;
   end
  h_s <= h;
  message_s <= message;
  message_words_s <= message_words;
 end





  // No sending new message flits when busy.
  assert property (disable iff (rst) 
                 @(posedge clk) busy |-> ~message_valid_s);
  // No sending h_valid_s and message_valid_s in the same cycle.
  assert property (disable iff (rst) 
                   @(posedge clk) ~(h_valid_s & message_valid_s));

  // This module is built around 4 instances of GF128 multipliers.
  localparam Dimension = 128;
  logic [3:0][Dimension-1:0] multiplier_operand_x;
  logic [3:0][Dimension-1:0] multiplier_operand_y;
  logic [3:0][Dimension-1:0] multiplier_result;
  logic [2*Dimension-2:0][Dimension-1:0] remainders;
  gf_mult_gmac_poly_remainders
    gf_mult_gmac_poly_remainders(
      .remainders(remainders));
  genvar gen_idx;
  generate
    for (gen_idx = 0; gen_idx < 4; gen_idx++) begin
      gf_multiply_remainder
        #(.Dimension(Dimension),
          .Swizzle(1))
        gf_multiply_remainder(
          .clk(clk),
          .rst(rst),
          .remainders(remainders),
          .operand_x(multiplier_operand_x[gen_idx]),
          .operand_y(multiplier_operand_y[gen_idx]),
          .result(multiplier_result[gen_idx]));
    end
  endgenerate

  // In the first two cycles including when h_valid_s is asserted, we push H, H^2 to
  // derive H, H^2, H^3, H^4.
  logic h_valid_plus_one;
  logic h_valid_plus_one_cp1;
  logic h_valid_plus_one_cp2;

  assign busy = h_valid_plus_one;

  always_ff @(posedge clk) begin : proc_h_valid_plus_one
    if (rst) begin
      h_valid_plus_one <= 1'b0;
      h_valid_plus_one_cp1 <= 1'b0;
      h_valid_plus_one_cp2 <= 1'b0;
    end else begin
      h_valid_plus_one <= h_valid_s;
      h_valid_plus_one_cp1 <= h_valid_s;
      h_valid_plus_one_cp2 <= h_valid_s;
    end
  end

  logic [3:0][127:0] h_powers;  // 0 => H, [1] => H^2, [2] => H^3
  logic [3:0][127:0] h_powers_shifted;
  logic [127:0]      h_powers0_cp; //duplicated h_powers[1] for timing fix.
  logic [127:0]      h_powers1_cp1; //duplicated h_powers[1] for timing fix.
  logic [127:0]      h_powers1_cp2; //duplicated h_powers[1] for timing fix.

  // If there is only one valid 128 bit input, multiplier 0 is used
  // hence
  always_comb begin: always_comb_h_powers_shifted
    case (message_words_s)
      2'b00: h_powers_shifted = h_powers << 3 * 128;  // 1 word
      2'b01: h_powers_shifted = h_powers << 2 * 128;  // 2 words
      2'b10: h_powers_shifted = h_powers << 1 * 128;  // 3 words
      default : h_powers_shifted = h_powers;          // 4 words
    endcase
  end
  // Drive inputs. Hardcoding this - makes things simpler.
  always_comb begin: always_comb_mult_inputs
    // Multiplier 0
    if (h_valid_s) begin  // compute H^2.
      multiplier_operand_x[0] = h_s;
      multiplier_operand_y[0] = h_s;
    end else begin
      multiplier_operand_x[0] = gmac ^ message_s[0];
      multiplier_operand_y[0] = h_powers_shifted[3];
    end

    multiplier_operand_x[1] = message_s[1];
    multiplier_operand_y[1] = h_powers_shifted[2];

    if (h_valid_plus_one) begin  // compute H^3
      multiplier_operand_x[2] = h_powers1_cp2; //h_powers[1];  // H^2
      multiplier_operand_y[2] = h_powers0_cp; //h_powers[0];
    end else begin
      multiplier_operand_x[2] = message_s[2];
      multiplier_operand_y[2] = h_powers_shifted[1];
    end

    if (h_valid_plus_one_cp1) begin // compute H^4.
      multiplier_operand_x[3] = h_powers1_cp1; //h_powers[1];  // H^2
      multiplier_operand_y[3] = h_powers1_cp1; //h_powers[1];  // H^2
    end else begin
      multiplier_operand_x[3] = message_s[3];
      multiplier_operand_y[3] = h_powers_shifted[0];
    end
  end

  // Latch h_powers
  always_ff @(posedge clk) begin : proc_h_powers
    if (h_valid_s) begin
      h_powers[0] <= h_s;
      h_powers0_cp <= h_s;
     end
    if (h_valid_s) begin
      h_powers[1] <= multiplier_result[0];
      h_powers1_cp1 <= multiplier_result[0]; //timing fix
      h_powers1_cp2 <= multiplier_result[0]; //timing fix
     end
    if (h_valid_plus_one_cp2) begin
      h_powers[2] <= multiplier_result[2];
      h_powers[3] <= multiplier_result[3];
    end
  end

  logic [127:0] xor_multiplier_results;
  logic [3:0][127:0] multiplier_masks;

  always_comb begin: always_comb_xor_reduce
    xor_multiplier_results = 128'd0;
    for (int idx = 0; idx < 4; idx++) begin
      multiplier_masks[idx] = (idx <= message_words_s) ? '1 : '0;
      xor_multiplier_results ^= (multiplier_masks[idx] & multiplier_result[idx]);
    end
  end

  // gmac and valid
  always_ff @(posedge clk) begin : proc_gmac
    if (rst) begin
      gmac <= 128'h0;
      gmac_valid <= 1'b0;
    end else begin
      if (h_valid_s) begin
        gmac_valid <= 1'b0;
        gmac <= 128'h0;
      end else begin
        gmac_valid <= message_valid_s;
        if (message_valid_s)
          gmac <= xor_multiplier_results;
      end
    end
  end

endmodule
