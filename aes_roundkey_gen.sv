/* Copyright 2019 Raghu Balasubramanian

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*/

module aes_roundkey_gen
  #(parameter KeyBytes=16)  // AES 128
  (
  input wire [KeyBytes-1:0][7:0] previous_roundkey,
  input wire [7:0] rcon, // rount constant for this round.
  input wire [3:0][7:0] rksel, 
  input wire is_g,
  output logic [KeyBytes-1:0][7:0] roundkey
);

logic [3:0][7:0] sbox_input;
logic [3:0][7:0] sbox_output;

sbox #(.NumParallelBytes(4))
  sbox
  (
    .input_bytes(sbox_input),
    .output_bytes(sbox_output)
  );

logic [31:0] temp;
//assign sbox_input = previous_roundkey[3:0];  // LSB 4 words
assign sbox_input = rksel;  // LSB 4 words
assign temp = is_g ?{sbox_output[3:0]}:
              {sbox_output[2:0], sbox_output[3]} ^ {rcon, 24'h0};
//            ^ rotated value                  xor rcon padded.
// temp = SubWord(RotWord(temp)) xor Rcon[i/Nk]

always_comb begin : always_comb_roundkey
  // w[i] = w[i-Nk] xor temp word by word.
  // Go MSB to LSB. ie., word 1 = [15:12]
  roundkey[15:12] = temp ^ previous_roundkey[15:12];
  roundkey[11:8] = roundkey[15:12] ^ previous_roundkey[11:8];
  roundkey[7:4] = roundkey[11:8] ^ previous_roundkey[7:4];
  roundkey[3:0] = roundkey[7:4] ^ previous_roundkey[3:0];
end

endmodule
