/* Copyright 2019 Raghu Balasubramanian

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*/

`timescale 1ns / 1ps

// Long form multiplication of two numbers in GF(2^dimension).
// This module is fully combinational (for now, may consider pipelining/flopping if we see timing
// issues).

module gf_multiply_remainder
  #(parameter integer Dimension = 128,
    parameter PipelineStage = 0,
    parameter Swizzle = 0)
  (
    input wire clk,
    input wire rst,

    // The lower half of this array will be unused. Retaining this for coding convenience.
    input wire [2*Dimension-2:0][Dimension-1:0] remainders,

    input wire [Dimension-1:0] operand_x,
    input wire [Dimension-1:0] operand_y,

    output logic [Dimension-1:0] result
  );

// Step 1, do a long form shifted XOR. This should give us a polynomial of
// degree 2*Dimension-2.
logic [2*Dimension-2:0] shifted_x_xor_y;
logic [2*Dimension-2:0] shifted_x_xor_y_f;

logic [Dimension-1:0] may_be_swizzled_operand_x;
logic [Dimension-1:0] may_be_swizzled_operand_y;
logic [Dimension-1:0] may_be_swizzled_result;

always_comb begin: always_comb_swizzle
  if (Swizzle == 1) begin
    for (int idx = 0; idx < 128; idx++) begin
      may_be_swizzled_operand_x[127-idx] = operand_x[idx];
      may_be_swizzled_operand_y[127-idx] = operand_y[idx];
      result[127-idx]                    = may_be_swizzled_result[idx];
    end
  end else begin
      may_be_swizzled_operand_x = operand_x;
      may_be_swizzled_operand_y = operand_y;
      result                    = may_be_swizzled_result;
  end
end

always_comb begin: always_comb_shifted_x_xor_y
  shifted_x_xor_y = 0;
  for (int idx = 0; idx < Dimension; idx++) begin
    shifted_x_xor_y ^= may_be_swizzled_operand_x[idx] ?
                          may_be_swizzled_operand_y << idx : '0;
  end
end: always_comb_shifted_x_xor_y

generate
  if (PipelineStage == 1) begin
    always_ff @(posedge clk) begin
      shifted_x_xor_y_f <= shifted_x_xor_y;
    end
  end
  else begin
    always_comb begin
      shifted_x_xor_y_f = shifted_x_xor_y;
    end
  end
endgenerate

always_comb begin: always_comb_reduce_with_remainders
  may_be_swizzled_result = shifted_x_xor_y_f[Dimension-1:0];
  for (int idx = Dimension; idx < 2*Dimension-1; idx++) begin
    may_be_swizzled_result ^= shifted_x_xor_y_f[idx] ? remainders[idx]: '0;
  end
end: always_comb_reduce_with_remainders

logic unused_signals_reduce;
always_comb begin
  unused_signals_reduce = 1'b0;
  for (int idx = 0; idx < Dimension; idx++) begin
    unused_signals_reduce |= |remainders[idx];
  end
end

endmodule // gf_multiply_remainder