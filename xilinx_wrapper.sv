/* Copyright 2019 Raghu Balasubramanian

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*/

`timescale 1ns / 1ps

// IOs that match constraints, just to help with synthesis and implementation.

module xilinx_wrapper
  #(parameter InputPipes=3,
    parameter OutputPipes=3,
    parameter DualInputShiftRegisters=1,
    parameter InputBusRegisters=1,
    parameter OutputBusRegisters=1,
    parameter InputWidth=256,
    parameter OutputWidth=256)
  (
  input wire clk,
  input wire rst,

  input wire [1:0] input_signal,

  output logic output_signal
);

logic [(InputPipes*2)-1:0]   input_signal_q;

logic [InputWidth-1:0] 	input_bus_a;
logic [InputWidth-1:0] 	input_bus_b;
logic [InputWidth-1:0] 	input_bus;

always_ff @(posedge clk) begin
  if (rst) begin
    input_signal_q <= '0;
    input_bus_a <= '0;
  end else begin
    input_signal_q <= {input_signal_q[InputPipes-3:0], input_signal};
    input_bus_a <= {input_bus_a[InputWidth-2:0], input_signal_q[InputPipes-1]};
  end
end


generate
  if (DualInputShiftRegisters) begin
    always_ff @(posedge clk) begin
      if (rst) begin
        input_bus_b <= '0;
      end else begin
         input_bus_b <= {input_signal_q[InputPipes-2], input_bus_b[InputWidth-1:1]};
      end
    end
  end // if (DualInputShiftRegisters)
  else begin
    assign input_bus_b = '0;
  end // else: !if(DualInputShiftRegisters)
endgenerate

generate
  if (InputBusRegisters) begin
    always_ff @(posedge clk) begin
      input_bus <= input_bus_a ^ input_bus_b;
    end
  end
  else begin
    assign input_bus = input_bus_a ^ input_bus_b;
  end
endgenerate

logic [InputWidth-1:0] 	output_bus;
logic [InputWidth-1:0] 	output_bus_q;

logic 			output_signal_d;
logic [OutputPipes-1:0] 	output_signal_q;

generate
  if (OutputBusRegisters) begin
    always_ff @(posedge clk) begin
      output_bus_q <= output_bus;
    end
  end
  else begin
    assign output_bus_q = output_bus;
  end
endgenerate

assign output_signal_d = ^output_bus_q;

always_ff @(posedge clk) begin
  if (rst) begin
    output_signal_q <= '0;
  end else begin
    output_signal_q <= {output_signal_q[OutputPipes-2:0], output_signal_d};
  end
end

assign output_signal = output_signal_q[OutputPipes-1];

localparam Dimension = 128;
logic [2*Dimension-1:0][Dimension-1:0] remainders;

// Use InputWidth for all internal signals.
gf_multiply_remainder
  gf_multiply_remainder(
    .clk(clk),
    .rst(rst),
    .remainders(remainders),
    .operand_x(input_bus[127:0]),
    .operand_y(input_bus[255:128]),
    .result(output_bus));

  gf_mult_gmac_poly_remainders
    gf_mult_gmac_poly_remainders(
      .remainders(remainders));

endmodule
