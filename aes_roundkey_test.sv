/* Copyright 2019 Raghu Balasubramanian

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*/

module aes_roundkey_test ();

parameter KeyBytes=16;
parameter NumRounds=11;

logic clk;
logic rst;

logic [NumRounds-1:0][KeyBytes-1:0][7:0] roundkeys;
logic [NumRounds-1:0][KeyBytes-1:0][7:0] expected_roundkeys; // [$];
logic start;
logic [KeyBytes-1:0][7:0] key;

initial begin
  clk = 1'b0;
  forever begin
    clk = #15 ~clk;
  end
end

initial begin
  rst = 1'b1;
  rst = #60 1'b0;
end

aes_roundkey #(
    .KeyBytes(KeyBytes),
    .NumRounds(NumRounds))
  dut(
    .clk(clk),
    .start(start),
    .key(key),
    .roundkeys(roundkeys));

// assign key = 128'h000102030405060708090a0b0c0d0e0f;
// assign key = 128'h00;
// assign key = 128'h62636363_62636363_62636363_62636363;
assign key = 128'h2b7e1516_28aed2a6_abf71588_09cf4f3c;

assign expected_roundkeys = {
      128'hd014f9a8c9ee2589e13f0cc8b6630ca6,
      128'hac7766f319fadc2128d12941575c006e,
      128'head27321b58dbad2312bf5607f8d292f,
      128'h4e54f70e5f5fc9f384a64fb24ea6dc4f,
      128'h6d88a37a110b3efddbf98641ca0093fd,
      128'hd4d1c6f87c839d87caf2b8bc11f915bc,
      128'hef44a541a8525b7fb671253bdb0bad00,
      128'h3d80477d4716fe3e1e237e446d7a883b,
      128'hf2c295f27a96b9435935807a7359f67f,
      128'ha0fafe1788542cb123a339392a6c7605,
      128'h2b7e151628aed2a6abf7158809cf4f3c};

// assign key = 128'h09cf4f3c_abf71588_28aed2a6_2b7e1516;

int free_running;

always_ff @(posedge clk) begin
  if (rst) free_running <= 0;
  else free_running <= free_running + 1;
end

assign start = free_running % 100 == 1;

// some cycles later (> NumRounds):
always_ff @(posedge clk) begin
  if (free_running % 100 == NumRounds+1) begin
    if (expected_roundkeys != roundkeys) begin
      $display("Expected = %x\n Got = %x", expected_roundkeys, roundkeys);
      $display("TEST FAILED");
      $finish;
    end
  end
  if (free_running == 10000) begin
      $display("TEST PASSED");
      $finish;
  end
end

endmodule