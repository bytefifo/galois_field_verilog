/* Copyright 2019 Raghu Balasubramanian

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*/

`timescale 1ns / 1ps

// SBox contains a 256* 8 bit array of hard coded SBox transforms.
// It takes in NumParallelBytes at a time and outputs the mapped byte for each
// one.

module sbox
  #(parameter NumParallelBytes = 4)
  (
    input wire [NumParallelBytes-1:0][7:0] input_bytes,
    output logic [NumParallelBytes-1:0][7:0] output_bytes
  );

wire [7:0] sbox [0:255];

always_comb begin: always_comb_outputs
  for (int idx = 0; idx < NumParallelBytes; idx++) begin
    output_bytes[idx] = sbox[input_bytes[idx]];
  end
end: always_comb_outputs

assign sbox['h00] = 8'h63;
assign sbox['h01] = 8'h7c;
assign sbox['h02] = 8'h77;
assign sbox['h03] = 8'h7b;
assign sbox['h04] = 8'hf2;
assign sbox['h05] = 8'h6b;
assign sbox['h06] = 8'h6f;
assign sbox['h07] = 8'hc5;
assign sbox['h08] = 8'h30;
assign sbox['h09] = 8'h01;
assign sbox['h0a] = 8'h67;
assign sbox['h0b] = 8'h2b;
assign sbox['h0c] = 8'hfe;
assign sbox['h0d] = 8'hd7;
assign sbox['h0e] = 8'hab;
assign sbox['h0f] = 8'h76;
assign sbox['h10] = 8'hca;
assign sbox['h11] = 8'h82;
assign sbox['h12] = 8'hc9;
assign sbox['h13] = 8'h7d;
assign sbox['h14] = 8'hfa;
assign sbox['h15] = 8'h59;
assign sbox['h16] = 8'h47;
assign sbox['h17] = 8'hf0;
assign sbox['h18] = 8'had;
assign sbox['h19] = 8'hd4;
assign sbox['h1a] = 8'ha2;
assign sbox['h1b] = 8'haf;
assign sbox['h1c] = 8'h9c;
assign sbox['h1d] = 8'ha4;
assign sbox['h1e] = 8'h72;
assign sbox['h1f] = 8'hc0;
assign sbox['h20] = 8'hb7;
assign sbox['h21] = 8'hfd;
assign sbox['h22] = 8'h93;
assign sbox['h23] = 8'h26;
assign sbox['h24] = 8'h36;
assign sbox['h25] = 8'h3f;
assign sbox['h26] = 8'hf7;
assign sbox['h27] = 8'hcc;
assign sbox['h28] = 8'h34;
assign sbox['h29] = 8'ha5;
assign sbox['h2a] = 8'he5;
assign sbox['h2b] = 8'hf1;
assign sbox['h2c] = 8'h71;
assign sbox['h2d] = 8'hd8;
assign sbox['h2e] = 8'h31;
assign sbox['h2f] = 8'h15;
assign sbox['h30] = 8'h04;
assign sbox['h31] = 8'hc7;
assign sbox['h32] = 8'h23;
assign sbox['h33] = 8'hc3;
assign sbox['h34] = 8'h18;
assign sbox['h35] = 8'h96;
assign sbox['h36] = 8'h05;
assign sbox['h37] = 8'h9a;
assign sbox['h38] = 8'h07;
assign sbox['h39] = 8'h12;
assign sbox['h3a] = 8'h80;
assign sbox['h3b] = 8'he2;
assign sbox['h3c] = 8'heb;
assign sbox['h3d] = 8'h27;
assign sbox['h3e] = 8'hb2;
assign sbox['h3f] = 8'h75;
assign sbox['h40] = 8'h09;
assign sbox['h41] = 8'h83;
assign sbox['h42] = 8'h2c;
assign sbox['h43] = 8'h1a;
assign sbox['h44] = 8'h1b;
assign sbox['h45] = 8'h6e;
assign sbox['h46] = 8'h5a;
assign sbox['h47] = 8'ha0;
assign sbox['h48] = 8'h52;
assign sbox['h49] = 8'h3b;
assign sbox['h4a] = 8'hd6;
assign sbox['h4b] = 8'hb3;
assign sbox['h4c] = 8'h29;
assign sbox['h4d] = 8'he3;
assign sbox['h4e] = 8'h2f;
assign sbox['h4f] = 8'h84;
assign sbox['h50] = 8'h53;
assign sbox['h51] = 8'hd1;
assign sbox['h52] = 8'h00;
assign sbox['h53] = 8'hed;
assign sbox['h54] = 8'h20;
assign sbox['h55] = 8'hfc;
assign sbox['h56] = 8'hb1;
assign sbox['h57] = 8'h5b;
assign sbox['h58] = 8'h6a;
assign sbox['h59] = 8'hcb;
assign sbox['h5a] = 8'hbe;
assign sbox['h5b] = 8'h39;
assign sbox['h5c] = 8'h4a;
assign sbox['h5d] = 8'h4c;
assign sbox['h5e] = 8'h58;
assign sbox['h5f] = 8'hcf;
assign sbox['h60] = 8'hd0;
assign sbox['h61] = 8'hef;
assign sbox['h62] = 8'haa;
assign sbox['h63] = 8'hfb;
assign sbox['h64] = 8'h43;
assign sbox['h65] = 8'h4d;
assign sbox['h66] = 8'h33;
assign sbox['h67] = 8'h85;
assign sbox['h68] = 8'h45;
assign sbox['h69] = 8'hf9;
assign sbox['h6a] = 8'h02;
assign sbox['h6b] = 8'h7f;
assign sbox['h6c] = 8'h50;
assign sbox['h6d] = 8'h3c;
assign sbox['h6e] = 8'h9f;
assign sbox['h6f] = 8'ha8;
assign sbox['h70] = 8'h51;
assign sbox['h71] = 8'ha3;
assign sbox['h72] = 8'h40;
assign sbox['h73] = 8'h8f;
assign sbox['h74] = 8'h92;
assign sbox['h75] = 8'h9d;
assign sbox['h76] = 8'h38;
assign sbox['h77] = 8'hf5;
assign sbox['h78] = 8'hbc;
assign sbox['h79] = 8'hb6;
assign sbox['h7a] = 8'hda;
assign sbox['h7b] = 8'h21;
assign sbox['h7c] = 8'h10;
assign sbox['h7d] = 8'hff;
assign sbox['h7e] = 8'hf3;
assign sbox['h7f] = 8'hd2;
assign sbox['h80] = 8'hcd;
assign sbox['h81] = 8'h0c;
assign sbox['h82] = 8'h13;
assign sbox['h83] = 8'hec;
assign sbox['h84] = 8'h5f;
assign sbox['h85] = 8'h97;
assign sbox['h86] = 8'h44;
assign sbox['h87] = 8'h17;
assign sbox['h88] = 8'hc4;
assign sbox['h89] = 8'ha7;
assign sbox['h8a] = 8'h7e;
assign sbox['h8b] = 8'h3d;
assign sbox['h8c] = 8'h64;
assign sbox['h8d] = 8'h5d;
assign sbox['h8e] = 8'h19;
assign sbox['h8f] = 8'h73;
assign sbox['h90] = 8'h60;
assign sbox['h91] = 8'h81;
assign sbox['h92] = 8'h4f;
assign sbox['h93] = 8'hdc;
assign sbox['h94] = 8'h22;
assign sbox['h95] = 8'h2a;
assign sbox['h96] = 8'h90;
assign sbox['h97] = 8'h88;
assign sbox['h98] = 8'h46;
assign sbox['h99] = 8'hee;
assign sbox['h9a] = 8'hb8;
assign sbox['h9b] = 8'h14;
assign sbox['h9c] = 8'hde;
assign sbox['h9d] = 8'h5e;
assign sbox['h9e] = 8'h0b;
assign sbox['h9f] = 8'hdb;
assign sbox['ha0] = 8'he0;
assign sbox['ha1] = 8'h32;
assign sbox['ha2] = 8'h3a;
assign sbox['ha3] = 8'h0a;
assign sbox['ha4] = 8'h49;
assign sbox['ha5] = 8'h06;
assign sbox['ha6] = 8'h24;
assign sbox['ha7] = 8'h5c;
assign sbox['ha8] = 8'hc2;
assign sbox['ha9] = 8'hd3;
assign sbox['haa] = 8'hac;
assign sbox['hab] = 8'h62;
assign sbox['hac] = 8'h91;
assign sbox['had] = 8'h95;
assign sbox['hae] = 8'he4;
assign sbox['haf] = 8'h79;
assign sbox['hb0] = 8'he7;
assign sbox['hb1] = 8'hc8;
assign sbox['hb2] = 8'h37;
assign sbox['hb3] = 8'h6d;
assign sbox['hb4] = 8'h8d;
assign sbox['hb5] = 8'hd5;
assign sbox['hb6] = 8'h4e;
assign sbox['hb7] = 8'ha9;
assign sbox['hb8] = 8'h6c;
assign sbox['hb9] = 8'h56;
assign sbox['hba] = 8'hf4;
assign sbox['hbb] = 8'hea;
assign sbox['hbc] = 8'h65;
assign sbox['hbd] = 8'h7a;
assign sbox['hbe] = 8'hae;
assign sbox['hbf] = 8'h08;
assign sbox['hc0] = 8'hba;
assign sbox['hc1] = 8'h78;
assign sbox['hc2] = 8'h25;
assign sbox['hc3] = 8'h2e;
assign sbox['hc4] = 8'h1c;
assign sbox['hc5] = 8'ha6;
assign sbox['hc6] = 8'hb4;
assign sbox['hc7] = 8'hc6;
assign sbox['hc8] = 8'he8;
assign sbox['hc9] = 8'hdd;
assign sbox['hca] = 8'h74;
assign sbox['hcb] = 8'h1f;
assign sbox['hcc] = 8'h4b;
assign sbox['hcd] = 8'hbd;
assign sbox['hce] = 8'h8b;
assign sbox['hcf] = 8'h8a;
assign sbox['hd0] = 8'h70;
assign sbox['hd1] = 8'h3e;
assign sbox['hd2] = 8'hb5;
assign sbox['hd3] = 8'h66;
assign sbox['hd4] = 8'h48;
assign sbox['hd5] = 8'h03;
assign sbox['hd6] = 8'hf6;
assign sbox['hd7] = 8'h0e;
assign sbox['hd8] = 8'h61;
assign sbox['hd9] = 8'h35;
assign sbox['hda] = 8'h57;
assign sbox['hdb] = 8'hb9;
assign sbox['hdc] = 8'h86;
assign sbox['hdd] = 8'hc1;
assign sbox['hde] = 8'h1d;
assign sbox['hdf] = 8'h9e;
assign sbox['he0] = 8'he1;
assign sbox['he1] = 8'hf8;
assign sbox['he2] = 8'h98;
assign sbox['he3] = 8'h11;
assign sbox['he4] = 8'h69;
assign sbox['he5] = 8'hd9;
assign sbox['he6] = 8'h8e;
assign sbox['he7] = 8'h94;
assign sbox['he8] = 8'h9b;
assign sbox['he9] = 8'h1e;
assign sbox['hea] = 8'h87;
assign sbox['heb] = 8'he9;
assign sbox['hec] = 8'hce;
assign sbox['hed] = 8'h55;
assign sbox['hee] = 8'h28;
assign sbox['hef] = 8'hdf;
assign sbox['hf0] = 8'h8c;
assign sbox['hf1] = 8'ha1;
assign sbox['hf2] = 8'h89;
assign sbox['hf3] = 8'h0d;
assign sbox['hf4] = 8'hbf;
assign sbox['hf5] = 8'he6;
assign sbox['hf6] = 8'h42;
assign sbox['hf7] = 8'h68;
assign sbox['hf8] = 8'h41;
assign sbox['hf9] = 8'h99;
assign sbox['hfa] = 8'h2d;
assign sbox['hfb] = 8'h0f;
assign sbox['hfc] = 8'hb0;
assign sbox['hfd] = 8'h54;
assign sbox['hfe] = 8'hbb;
assign sbox['hff] = 8'h16;

endmodule