/* Copyright 2019 Raghu Balasubramanian

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*/

package aes_pkg;

typedef enum integer {
  aes_128,
  aes_196,
  aes_256
} aes_type_t;

typedef enum logic [1:0] {
  key = 0,
  iv = 1,
  auth = 2,
  payload = 3
} whats_in_data_t;

// word = 32 bits.
typedef struct packed{
  // Last word in the payload. (first word == when iv is 1).
  logic is_last;
  whats_in_data_t whats_in_data;
  // 0 => 1 byte is valid.
  // 2 => 2 bytes are valid...
  // Must be 15 when key, 2 when iv.
  logic [$clog2(16*4)-1:0] valid_bytes;
  // word 0 is the first four word.
  // key must be in message[0][127:0] - byte [127:120] is key byte 0.
  // iv must be in message[0][95:0] - byte [95:88] is IV byte 0.
  // The endianness in data is iky. message 0 is the first 128 bits.
  // in that, bits 127:120 is the first byte, 7:0 is byte 15.
  logic [3:0][127:0] message;
} data_t;

function integer NumStages(aes_type_t aes_type);
  NumStages = aes_type == aes_128 ? 11:
              aes_type == aes_196 ? 13: 15;  // TODO fix these.
          //    aes_type == aes_196 ? 16: 19;  // TODO fix these.
  return NumStages;
endfunction : NumStages

endpackage
